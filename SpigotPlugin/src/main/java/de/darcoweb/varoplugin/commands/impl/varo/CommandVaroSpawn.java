package de.darcoweb.varoplugin.commands.impl.varo;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.commands.SuperSubCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.GameState;
import de.darcoweb.varoplugin.utilities.Message;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroSpawn extends SuperSubCommand<VaroPlugin> {

    public CommandVaroSpawn(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "spawn", "Manages the spawns for all players");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof Player))
            Chat.err.send(sender, Message.general.NOT_INGAME());
        else if (!rootCommand.getPlugin().isState(GameState.PREPARATION))
            Chat.err.send(sender, Message.game.ALREADY_STARTED());
        else {
            if (args.length == 0)
                // TODO Show spawns via Packets? Or just use plain old normal armorstands for every admin to see?
                // Maybe set args[0] to show or auto ? to be decided
                Chat.err.send(sender, "UNIMPLEMENTED");
            else
                super.execute(sender, args);
        }
    }
}
