package de.darcoweb.varoplugin.event;

import org.bukkit.block.Block;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.inventory.BrewerInventory;

public class StrengthenBrewEvent extends BrewEvent {

	public StrengthenBrewEvent(Block brewer, BrewerInventory contents) {
		super(brewer, contents);
	}
	
	private static final HandlerList handlers = new HandlerList();
	 
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}
}
