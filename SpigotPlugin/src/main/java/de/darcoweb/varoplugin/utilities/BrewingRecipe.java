package de.darcoweb.varoplugin.utilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

/**
 * This class is just a data class to make it easier to store all the different brewing recipes.
 * 
 * @author Marco
 */
public class BrewingRecipe implements Recipe {

	public static Set<BrewingRecipe> recipes = new HashSet<BrewingRecipe>();

	public static List<BrewingRecipe> getRecipesForIngredient(ItemStack i) {
		List<BrewingRecipe> matchingRecipes = new ArrayList<BrewingRecipe>();

		if (recipes.isEmpty())
			return matchingRecipes;
		
		for (BrewingRecipe recipe : recipes) {
			ItemStack ingredient = recipe.getIngredient();
			
			// Should I add Meta data comparison? TODO
			if (i.getType().equals(ingredient.getType())) {
				matchingRecipes.add(recipe);
			}
		}
		
		return matchingRecipes;
	}

	private ItemStack result;
	private ItemStack ingredient;
	private ItemStack base;

	public BrewingRecipe(ItemStack base, ItemStack ingr, ItemStack result) {
		this.base = base;
		this.ingredient = ingr;
		this.result = result;
	}

	@Override
	public ItemStack getResult() {
		return this.result;
	}

	public ItemStack getIngredient() {
		return this.ingredient;
	}

	public ItemStack getBase() {
		return this.base;
	}
}
