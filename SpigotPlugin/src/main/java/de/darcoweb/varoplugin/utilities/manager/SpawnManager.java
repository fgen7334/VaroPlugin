package de.darcoweb.varoplugin.utilities.manager;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.utilities.LocationUtils;
import de.darcoweb.varoplugin.utilities.MarkedLocation;
import de.darcoweb.varoplugin.utilities.Spawnpoint;
import lombok.Getter;
import lombok.experimental.ExtensionMethod;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
@ExtensionMethod(LocationUtils.EntityUtils.class)
public class SpawnManager {

    public static final String CENTER_NAME =
            "§f§k§l###§r" + ChatColor.GOLD + " §lMap-Center " + "§r§k§l###";

    private final VaroPlugin plugin;

    @Getter
    private final MarkedLocation.MutableMarkedLocation center = new MarkedLocation.MutableMarkedLocation();

    @Getter
    private final Set<Spawnpoint> spawnpoints = new TreeSet<>();

    // TODO This whole animation falling down gravity things relies on the face that there won't be any blocks above
    // TODO the spawn area... if someone wants to start in a cave, e.g, at least a config option is needed!
    private BukkitTask onGroundListener = null;
    private List<ArmorStand> trackedArmorStands = new ArrayList<>();

    public SpawnManager(VaroPlugin plugin) {
        this.plugin = plugin;
        loadSpawns();
    }

    /**
     * Adds a {@link Location} to an internal list, then summons an {@link ArmorStand} marker (with an animation)
     * and tells the caller about the id of that spawn-point (first spawn has the id 1).
     *
     * @param location The location of the spawn to be added
     * @param inHole   Whether the ArmorStand falls into a hole
     * @return The id of the just added spawn-point (counting from 1)
     */
    public int addSpawn(Location location, boolean inHole) {
        // Since the element will be added next, its index is the size of the list (+1 bc/ we're starting at1 to not have a spawn 0)
        ArmorStand armorStand = newArmorStand(location);
        Spawnpoint spawnpoint = new Spawnpoint(spawnpoints.size() + 1, location, armorStand.getUniqueId());

        animateArmorStand(armorStand, spawnpoint.getNametag(), inHole);

        spawnpoints.add(spawnpoint);
        return spawnpoint.getId();
    }

    public int removeLastSpawnpoint() {
        if (spawnpoints.isEmpty()) return -1;
        Optional<Spawnpoint> last = spawnpoints.stream().filter(spawnpoint -> spawnpoint.getId() == spawnpoints.size()).findFirst();
        last.ifPresent(spawnpoints::remove);
        last.ifPresent(spawnpoint -> spawnpoint.getLocation().getWorld().getEntities().stream()
                .filter(entity -> entity.getUniqueId().equals(spawnpoint.getArmorStandUuid())).findFirst()
                .ifPresent(Entity::remove));

        return last.isPresent() ? last.get().getId() : -1;
    }

    public void updateCenter(Location newCenter) {
        center.setLocation(newCenter);

        if (center.isArmorStandUuidSet()) removeArmorStandEntity(center.immutableCopy());

        center.setArmorStandUuid(animateArmorStand(newArmorStand(newCenter), CENTER_NAME, false).getUniqueId());
    }

    // TODO Minecraft doesn't like spawning armorstands, then teleporting them up and then letting them fall
    // TODO these armorstands seem to glitch through the floor but then reappear at the correct position
    private ArmorStand animateArmorStand(ArmorStand armorStand, String name, final boolean inHole) {
        armorStand.teleport(armorStand.getLocation().add(0, 15, 0));

        armorStand.setVisible(false);
        armorStand.setArms(inHole); // Used to keep track of the inHole setting for each individual armor stand.
        //armorStand.setMetadata("varo-preparation", no idea how this is supposed to work);
        armorStand.setCustomName(name);
        armorStand.setCustomNameVisible(true);

        trackedArmorStands.add(armorStand);

        if (onGroundListener != null) return armorStand;
        onGroundListener = plugin.getServer().getScheduler().runTaskTimer(plugin, () -> {
            Iterator<ArmorStand> iterator = trackedArmorStands.iterator();
            while (iterator.hasNext()) {
                ArmorStand trackedArmorStand = iterator.next();
                if (!trackedArmorStand.isOnGround()) continue;

                // With @ExtensionMethod use:
                // trackedArmorStand.makeMarker();
                LocationUtils.EntityUtils.makeMarker(trackedArmorStand);
                trackedArmorStand.teleport(trackedArmorStand.getLocation().add(0, trackedArmorStand.hasArms() ? 2 : 1, 0));

                iterator.remove();

                if (trackedArmorStands.isEmpty()) {
                    onGroundListener.cancel();
                    onGroundListener = null;
                }
            }
        }, 0, 1L);

        return armorStand;
    }

    private ArmorStand makeMarker(ArmorStand armorStand, String name) {
        armorStand.setVisible(false);
        armorStand.setGravity(false);
        armorStand.setMarker(true);

        armorStand.setCustomName(name);
        armorStand.setCustomNameVisible(true);

        return armorStand;
    }

    private ArmorStand newArmorStand(Location location) {
        return (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
    }

    private void removeArmorStandEntity(MarkedLocation point) {
        point.getLocation().getWorld().getEntitiesByClass(ArmorStand.class).stream().filter(armorStand ->
                armorStand.getUniqueId().equals(point.getArmorStandUuid())).findFirst().ifPresent(ArmorStand::remove);
    }

    // ================================================================================

    public void loadSpawns() {
        List<String> locationStrings = plugin.getFileManager().getFileConfig(FileManager.YamlFile.VARO_DATA).getStringList("spawnpoints");
        if (locationStrings.isEmpty()) return;

        for (String locationString : locationStrings) {
            Spawnpoint spawnpoint = Spawnpoint.fromSerializedString(locationString);

            spawnpoint.getLocation().getWorld().getEntitiesByClass(ArmorStand.class)
                    .stream().filter(entity -> entity.getUniqueId().equals(spawnpoint.getArmorStandUuid())).findFirst()
                    .orElseGet(() -> makeMarker(newArmorStand(spawnpoint.getLocation()), spawnpoint.getNametag()));

            spawnpoints.add(spawnpoint);
        }
    }

    public void saveSpawns() {
        plugin.getFileManager().getFileConfig(FileManager.YamlFile.VARO_DATA).set("spawnpoints",
                spawnpoints.stream().map(Spawnpoint::toSerializedString).collect(Collectors.toList()));
    }
}
