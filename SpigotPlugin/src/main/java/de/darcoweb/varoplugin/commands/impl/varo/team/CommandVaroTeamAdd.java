package de.darcoweb.varoplugin.commands.impl.varo.team;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import de.darcoweb.varoplugin.utilities.manager.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

import java.util.HashSet;
import java.util.Set;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroTeamAdd extends SubCommand<VaroPlugin> {

    public CommandVaroTeamAdd(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "add", "Creates a new team", "Name", "#Players...");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 1) {
            Chat.err.send(sender, Message.general.INVALID_ARGUMENTS());
            return;
        }

        TeamManager teamManager = rootCommand.getPlugin().getTeamManager();
        Set<OfflinePlayer> playerSet = new HashSet<>();
        for (int i = 1; i < args.length; i++) {
            // Using a deprecated method here if totally fine, as
            // the user executing the command can't type the UUID of
            // the player. Next, the playername isn't getting stored
            // anywhere. I just use the Name to create a new
            // Player-Instance so the TeamManager can store their UUID.
            playerSet.add(Bukkit.getOfflinePlayer(args[i]));
        }

        teamManager.newTeam(args[0], playerSet);
        Chat.success.send(sender, Message.team.WAS_CREATED(args[0]));
    }
}
