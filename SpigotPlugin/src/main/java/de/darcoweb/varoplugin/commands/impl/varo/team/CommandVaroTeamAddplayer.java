package de.darcoweb.varoplugin.commands.impl.varo.team;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import de.darcoweb.varoplugin.utilities.Team;
import de.darcoweb.varoplugin.utilities.manager.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroTeamAddplayer extends SubCommand<VaroPlugin> {

    public CommandVaroTeamAddplayer(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "addplayer", "Add a player to an existing team", "Team", "Player");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 2) {
            Chat.err.send(sender, Message.general.INVALID_ARGUMENTS());
            return;
        }

        TeamManager teamManager = rootCommand.getPlugin().getTeamManager();
        Team t = teamManager.getTeam(args[0]);
        if (t != null) {
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[1]);
            Team oldTeam = teamManager.getPlayersTeam(offlinePlayer);
            if (oldTeam != null) {
                oldTeam.removePlayer(offlinePlayer.getUniqueId());
                Chat.success.send(sender, Message.team.PLAYER_REMOVED(offlinePlayer.getName(), oldTeam.getName()));
            }

            t.addPlayer(offlinePlayer.getUniqueId());
            Chat.success.send(sender, Message.team.PLAYER_ADDED(offlinePlayer.getName(), t.getName()));
        } else {
            Chat.err.send(sender, Message.team.NOT_EXISTS());
        }
    }
}
