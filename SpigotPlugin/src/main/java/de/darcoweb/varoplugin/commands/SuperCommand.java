package de.darcoweb.varoplugin.commands;

import java.util.List;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public interface SuperCommand {

    String getName();

    List<SubCommand> getSubCommands();

    default void registerSubCommand(SubCommand newCommand) {
        if (getSubCommands().stream().map(SubCommand::getName)
                .anyMatch(newCommand.getName()::equalsIgnoreCase)) {

            String exceptionMessage = "A sub-command with the name " + newCommand.getName() +
                    " has already been registered for the %sCommand '/%s' !";
            if (this instanceof SubCommand)
                exceptionMessage = String.format(exceptionMessage, "", ((SubCommand) this).getFullCommand());
            else
                exceptionMessage = String.format(exceptionMessage, "Root", getName());

            throw new IllegalArgumentException(exceptionMessage);
        }

        getSubCommands().add(newCommand);
    }
}
