package de.darcoweb.varoplugin.annotations.processors;

import de.darcoweb.varoplugin.annotations.MessageList;
import de.darcoweb.varoplugin.annotations.impl.Message;
import de.darcoweb.varoplugin.annotations.impl.MessageInfo;
import org.apache.commons.lang.WordUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.*;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

/**
 * Processes the {@link de.darcoweb.varoplugin.annotations.MessageList @MessageList}
 * annotation and generates a message enum.
 *
 * @author 1Darco1
 */
@SupportedAnnotationTypes("de.darcoweb.varoplugin.annotations.MessageList")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class MessageListProcessor extends AbstractProcessor {

    private final boolean debug = true;

    public MessageListProcessor() {
        super();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        // Iterates over the elements annotated with the given annotation
        for (Element e : roundEnv.getElementsAnnotatedWith(MessageList.class)) {
            if (!(e.getKind() == ElementKind.ENUM)) {
                raiseErrorAt(e, "Can only annotate enum types");
                continue;
            }

            TypeElement element = (TypeElement) e;
            try {
                ClassLoader classLoader = getClass().getClassLoader();

                debug(getCurrentClasspath()
                        .replaceAll(":", "\n")
                        .replaceAll("/Users/Marco/.m2/repository/", ""));

                Class<MessageInfo> messageInfoClass = MessageInfo.class;

                // Can NOT load a class that is not compiled yet.
                // The annotation process happens before the annotated class is finally compiled
                // See: http://notatube.blogspot.de/2010/11/project-lombok-trick-explained.html

                // Well, then, lets compile the class(es):
                JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
                StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);

                Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjects(
                        new File("SpigotPlugin/" + convertToPath(element.getQualifiedName().toString())));

                String classpath = getCurrentClasspath(false) +
                        new File("Annotations/target/Annotations.jar").getAbsolutePath();

                File outputDir = new File("SpigotPlugin/target/classes/");

                Iterable<String> arguments = Arrays.asList("-proc:none",
                        "-d", outputDir.getAbsolutePath(),
                        "-classpath", classpath);

                boolean success = compiler.getTask(null, fileManager, null, arguments, null, compilationUnits).call();

                fileManager.close();

                if (!success) {
                    raiseErrorAt(e, "Compile time error whilst compiling annotated class(es)! Please check.\n" +
                            "THE ANNOTATED CLASS COULD NOT BE COMPILED PROPERLY!");
                    continue;
                }

                debug("Compiled annotated classes..");

                // Okay, so now load the classes via reflection:
                // First, initialize a class loader that searches the outputDir...
                URL classesURL = new URL("file://" + outputDir.getAbsolutePath() + "/");
                URLClassLoader customCL = URLClassLoader.newInstance(new URL[]{classesURL}, classLoader);

                Class<?> annotatedClass = customCL.loadClass(element.getQualifiedName().toString());

                debug("Loaded Classes..");

                // NOW, continue:
                if (!messageInfoClass.isAssignableFrom(annotatedClass)) {
                    raiseErrorAt(element, "Can only annotate subclasses of MessageInfo");
                    continue;
                }

                // Read all the info associated with the annotation
                MessageList annotation = element.getAnnotation(MessageList.class);
                String locals = annotation.value();
                String destinationPackage = annotation.destinationPackage();

                // String destinationPackage() default "";
                if (destinationPackage.equals("")) {
                    Element enclosingElement = element;
                    while (!((enclosingElement = enclosingElement.getEnclosingElement()) instanceof PackageElement)) ;
                    destinationPackage = ((PackageElement) enclosingElement).getQualifiedName().toString();
                }

                Map<String, List<Message>> messageCategories = new HashMap<>();
                for (Field field : annotatedClass.getDeclaredFields()) {
                    if (!field.isEnumConstant()) continue;

                    String categoryId = field.getName().replaceFirst("_", ":").split(":")[0];

                    // Enum constants are static:
                    Object value = field.get(null);
                    MessageInfo messageInfo = (MessageInfo) value;
                    Message message = new Message(
                            field.getName().replaceFirst(categoryId + "_", ""),
                            messageInfo.getKey(),
                            messageInfo.getParams());

                    String categoryNameKey = WordUtils.capitalizeFully(categoryId);
                    if (!messageCategories.containsKey(categoryNameKey))
                        messageCategories.put(categoryNameKey, new ArrayList<>());

                    messageCategories.get(categoryNameKey).add(message);
                }

                debug("Extracted infos..");

                // Initialize the VelocityContext, pass all parameters and write the whole thing to a file.
                Properties props = new Properties();
                props.load(this.getClass().getClassLoader().getResourceAsStream("velocity.properties"));

                VelocityEngine velocityEngine = new VelocityEngine(props);
                velocityEngine.init();

                /*ToolManager toolManager = new ToolManager();
                toolManager.setVelocityEngine(velocityEngine);
                toolManager.configure("velocity-tools.xml");*/

                VelocityContext context = new VelocityContext();
                context.put("package", destinationPackage);
                context.put("fqLocals", locals);
                context.put("locals", locals.substring(locals.lastIndexOf(".") + 1, locals.length()));
                context.put("messageCategories", messageCategories);

                Writer writer = processingEnv.getFiler().createSourceFile("Message").openWriter();
                Template template = velocityEngine.getTemplate("MessageTemplate.vm");
                template.merge(context, writer);
                writer.close();

            } catch (ClassNotFoundException | IllegalAccessException | IOException exception) {
                raiseErrorAt(element, "[FATAL] " + exception.getClass().getSimpleName() + ": " + exception.getMessage());
                exception.printStackTrace();
            }
        }
        return true;
    }

    private void raiseErrorAt(Element e, String message) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, message, e);
    }

    private void debug(String msg) {
        if (debug) System.out.println(msg);
    }

    private String convertToPath(String rawQualifiedType) {
        return "src/main/java/" + rawQualifiedType.replace('.', '/') + ".java";
    }

    private String getCurrentClasspath() {
        return getCurrentClasspath(true);
    }

    private String getCurrentClasspath(boolean trim) {
        StringBuilder builder = new StringBuilder();
        for (URL url : ((URLClassLoader) Thread.currentThread().getContextClassLoader()).getURLs()) {
            builder.append(new File(url.getPath()));
            builder.append(System.getProperty("path.separator"));
        }
        String classpath = builder.toString();
        return trim ? classpath.substring(0, classpath.length() - 1) : classpath;
    }
}
