package de.darcoweb.varoplugin.utilities.manager;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.utilities.Team;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;

import java.util.*;
import java.util.stream.Collectors;

public class TeamManager {

    private List<Team> teams = new ArrayList<>();

    /**
     * UNUSED until 1.8
     */
    private Set<OfflinePlayer> specs;

    public TeamManager() {
        loadTeams();
    }

    @SuppressWarnings("unchecked")
    public void loadTeams() {
        FileManager fileManager = VaroPlugin.getInstance().getFileManager();
        if (fileManager.contains(FileManager.YamlFile.TEAMS, "teamnames")) {
            List<String> teamnames = (List<String>) fileManager.getFileConfig(FileManager.YamlFile.TEAMS).getList("teamnames");

            if (!teamnames.isEmpty()) {
                for (String teamname : teamnames) {
                    String colorChar = String.valueOf(fileManager.get(FileManager.YamlFile.TEAMS, "teams." + teamname + ".color"));
                    ChatColor color = ChatColor.getByChar(colorChar.charAt(0));

                    Set<OfflinePlayer> players = new HashSet<>();
                    if (fileManager.contains(FileManager.YamlFile.TEAMS, "teams." + teamname + ".players")) {
                        List<String> playeruuids = (List<String>) fileManager.getFileConfig(FileManager.YamlFile.TEAMS).getList(
                                "teams." + teamname + ".players");
                        players.addAll(playeruuids.stream()
                                .map(uuid -> Bukkit.getOfflinePlayer(UUID.fromString(uuid)))
                                .collect(Collectors.toList()));
                    }

                    Location chestLoc = null;
                    if (fileManager.contains(FileManager.YamlFile.TEAMS, "teams." + teamname + ".locked_chest")) {
                        String locString = (String) fileManager.get(FileManager.YamlFile.TEAMS, "teams." + teamname + ".locked_chest");
                        String[] splitLoc = locString.split(",");
                        chestLoc = new Location(Bukkit.getWorld(splitLoc[0]), Integer.parseInt(splitLoc[1]),
                                Integer.parseInt(splitLoc[2]), Integer.parseInt(splitLoc[3]));
                    }

                    Team t = newTeam(teamname, players);
                    t.setColor(color);
                    t.setChestLocation(chestLoc);
                    t.updateAlive();
                }
            }
        }
    }

    public Team newTeam(String name, Set<OfflinePlayer> players) {
        Team team = new Team(name);
        if (!players.isEmpty()) {
            players.forEach(player -> team.addPlayer(player.getUniqueId()));
        }
        team.setColor(ChatColor.WHITE);
        team.updateAlive();
        teams.add(team);
        saveTeams();
        return team;
    }

    public Team getPlayersTeam(OfflinePlayer p) {
        Team team = null;
        if (!teams.isEmpty()) {
            for (Team t : teams) {
                if (t.hasPlayer(p.getUniqueId())) {
                    team = t;
                    break;
                }
            }
        }
        return team;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public List<Team> getAliveTeams() {
        List<Team> aliveTeams = new ArrayList<>();
        if (!this.teams.isEmpty()) {
            for (Team t : this.teams) {
                if (t.isAlive())
                    aliveTeams.add(t);
            }
        }
        return aliveTeams;
    }

    public void remove(Team team) {
        teams.remove(team);
    }

    public Team getTeam(String name) {
        Team t = null;
        if (!teams.isEmpty()) {
            for (Team team : teams) {
                if (team.getName().equalsIgnoreCase(name)) {
                    t = team;
                }
            }
        }
        return t;
    }

    public Set<Location> getAllChests() {
        Set<Location> locs = new HashSet<>();
        for (Team t : teams) {
            locs.add(t.getChestLocation());
        }
        return locs;
    }

    public void saveTeams() {
        FileManager fileManager = VaroPlugin.getInstance().getFileManager();
        fileManager.freshConfig(FileManager.YamlFile.TEAMS);

        List<String> teamnames = new ArrayList<>();
        if (!teams.isEmpty()) {
            for (Team team : teams) {
                teamnames.add(team.getName());
                String path = "teams." + team.getName();

                // Color
                char color = team.getColor().getChar();
                fileManager.write(FileManager.YamlFile.TEAMS, path + ".color", color);

                // Players
                if (team.getSize() > 0) {
                    List<String> players = team.getUuids().stream().map(UUID::toString).collect(Collectors.toList());
                    fileManager.write(FileManager.YamlFile.TEAMS, path + ".players", players);
                }

                // Chest
                Location chestLocation = team.getChestLocation();
                if (chestLocation != null) {
                    fileManager.write(FileManager.YamlFile.TEAMS, path + ".locked_chest",
                            chestLocation.getWorld().getName() + "," + chestLocation.getBlockX() + ","
                            + chestLocation.getBlockY() + "," + chestLocation.getBlockZ());
                }
            }
            fileManager.write(FileManager.YamlFile.TEAMS, "teamnames", teamnames);
        }
    }

    // #############################################
    // # Spectators wont be implemented until 1.8! #
    // #############################################
    public void addSpectator(OfflinePlayer p) {
        specs.add(p);
    }

    public Set<OfflinePlayer> getSpectators() {
        return specs;
    }

    public boolean isSpectator(OfflinePlayer p) {
        return specs.contains(p);
    }
}
