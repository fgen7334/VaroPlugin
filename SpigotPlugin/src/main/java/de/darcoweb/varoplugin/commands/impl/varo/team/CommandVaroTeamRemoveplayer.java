package de.darcoweb.varoplugin.commands.impl.varo.team;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import de.darcoweb.varoplugin.utilities.Team;
import de.darcoweb.varoplugin.utilities.manager.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroTeamRemoveplayer extends SubCommand<VaroPlugin> {

    public CommandVaroTeamRemoveplayer(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "removeplayer", "Removes a player from their team", "Player");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 1) {
            Chat.err.send(sender, Message.general.INVALID_ARGUMENTS());
            return;
        }

        TeamManager teamManager = rootCommand.getPlugin().getTeamManager();
        Team team = teamManager.getPlayersTeam(Bukkit.getOfflinePlayer(args[0]));
        if (team == null) {
            Chat.err.send(sender, Message.team.PLAYER_NOT_IN_TEAM(args[0], "null"));
            return;
        }

        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);
        team.removePlayer(offlinePlayer.getUniqueId());
        Chat.success.send(sender, Message.team.PLAYER_REMOVED(offlinePlayer.getName(), team.getName()));
    }
}
