package de.darcoweb.varoplugin.commands.impl;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Team;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;

public class CommandTeam extends Command<VaroPlugin> {

    protected CommandTeam(VaroPlugin plugin) {
        super(plugin, "team", "Displays information about your team", "t");
    }

    // TODO: Make things prettier.
    // TODO: Multiple pages if amount of items to display is greater than 54...
    // TODO: Add lore with details
    // TODO: Add one item for each team to display general info ?

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (!(sender instanceof Player)) {
            Chat.err.send(sender, "Du kannst das Info-View nicht sehen!");
            return true;
        }

        Player p = (Player) sender;

        p.openInventory(getInfoInventory());

        return true;
    }

    @SuppressWarnings("unused")
    private void sendInfoMessage(Player p, Team t) {
        p.sendMessage(ChatColor.DARK_PURPLE + "Du bist in dem Team " + t.getName() + " mit " + t.getSize()
                + " Spielern:");
        for (OfflinePlayer player : t.getPlayers()) {
            p.sendMessage(ChatColor.GOLD + "   # " + (VaroPlugin.getInstance().getBanManager().isBanned(p) ? "(tot)" : "")
                    + player.getName());
        }
    }

    private Inventory getInfoInventory() {
        List<ItemStack> items = generateInventoryItems(VaroPlugin.getInstance().getTeamManager().getTeams());

        int rows = (int) Math.ceil((double) (items.size()) / 9.0);
        Inventory inv = Bukkit.createInventory(null, rows * 9, "Alle Spieler");

        for (int i = 0; i < items.size(); i++) {
            inv.setItem(i, items.get(i));
        }

        return inv;
    }

    private List<ItemStack> generateInventoryItems(Iterable<Team> teams) {
        List<ItemStack> items = new ArrayList<>();
        // TeamManager tm = TeamManager.getInstance();

        for (Team t : teams) {
            for (OfflinePlayer p : t.getPlayers()) {
                ItemStack i = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
                SkullMeta meta = (SkullMeta) i.getItemMeta();
                meta.setOwner(p.getName());
                meta.setDisplayName(ChatColor.DARK_GRAY + "[" + ChatColor.GRAY + t.getName() + ChatColor.DARK_GRAY
                        + "] " + t.getColor() + p.getName());
                i.setItemMeta(meta);

                items.add(i);
            }

            ItemStack pane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
            ItemMeta meta = pane.getItemMeta();
            meta.setDisplayName(" ");
            pane.setItemMeta(meta);
            items.add(pane);
        }

        return items;
    }
}
