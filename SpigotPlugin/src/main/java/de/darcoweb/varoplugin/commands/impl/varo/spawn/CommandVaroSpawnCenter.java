package de.darcoweb.varoplugin.commands.impl.varo.spawn;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.LocationUtils;
import de.darcoweb.varoplugin.utilities.Message;
import de.darcoweb.varoplugin.utilities.StringUtils;
import lombok.experimental.ExtensionMethod;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
@ExtensionMethod(LocationUtils.class) // TODO
public class CommandVaroSpawnCenter extends SubCommand<VaroPlugin> {

    public CommandVaroSpawnCenter(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "center", "Sets the center point of the map", "#X", "#Y", "#Z");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        Location location;
        Player player = (Player) sender;
        if (args.length < 3)
            // To be replaced with the following using ExtensionMethod
            // location = player.getLocation().getBlockCenter();
            location = LocationUtils.getBlockCenter(player.getLocation());
        else if (args.length > 3) {
            Chat.err.send(sender, Message.general.INVALID_ARGUMENTS());
            return;
        } else {
            List<Integer> coordinates = new ArrayList<>();
            for (String argument : args) {
                Optional<Integer> parsed = StringUtils.parseMinecraftBlockCoord(argument);
                if (!parsed.isPresent()) {
                    Chat.err.send(sender, Message.general.PROVIDE_WHOLE_NUMBER());
                    return;
                }

                coordinates.add(parsed.get());
            }

            // Using ExtensionMethod, just add .getCenter() after the instantiation.
            // location = new Location(player.getWorld(), coordinates.get(0), coordinates.get(1), coordinates.get(2)).getCenter();
            location = LocationUtils.getBlockCenter(
                    new Location(player.getWorld(), coordinates.get(0), coordinates.get(1), coordinates.get(2))
            );
        }

        rootCommand.getPlugin().getSpawnManager().updateCenter(location);
        Chat.success.send(sender, Message.spawn.CENTER_SET_SUCCESS());
    }
}
