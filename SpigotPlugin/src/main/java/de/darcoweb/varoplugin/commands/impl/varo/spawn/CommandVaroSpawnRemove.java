package de.darcoweb.varoplugin.commands.impl.varo.spawn;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import org.bukkit.command.CommandSender;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroSpawnRemove extends SubCommand<VaroPlugin> {

    public CommandVaroSpawnRemove(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "remove", "Removes the spawnpoint with the greatest id");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length > 1) {
            Chat.err.send(sender, Message.spawn.PROBABLY_USE_REPLACE());
            return;
        }

        int id = rootCommand.getPlugin().getSpawnManager().removeLastSpawnpoint();
        if (id > 0)
            Chat.success.send(sender, Message.spawn.REMOVED(String.valueOf(id)));
        else
            Chat.err.send(sender, Message.spawn.NO_SPAWNPOINT_SET());
    }
}
