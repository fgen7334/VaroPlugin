package de.darcoweb.varoplugin.commands.impl.varo;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import org.bukkit.command.CommandSender;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroReload extends SubCommand<VaroPlugin> {

    public CommandVaroReload(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "reload", "Reloads the config.yml file");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        rootCommand.getPlugin().reloadConfig();
        Chat.success.opBroadcast(Message.config.RELOADED());
    }
}
