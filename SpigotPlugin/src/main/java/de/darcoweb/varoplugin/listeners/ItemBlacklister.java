package de.darcoweb.varoplugin.listeners;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.event.ExtendBrewEvent;
import de.darcoweb.varoplugin.event.StrengthenBrewEvent;
import de.darcoweb.varoplugin.event.ThrowableBrewEvent;
import de.darcoweb.varoplugin.utilities.BrewingRecipe;
import de.darcoweb.varoplugin.utilities.Chat;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.BrewerInventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffectType;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;

public class ItemBlacklister /*implements Listener*/ {

    private final int POTION_SLOTS = 3;

    private List<ItemStack> items = new ArrayList<>();
    private PotionBlacklist potions = new PotionBlacklist();

    public ItemBlacklister() {
        readCraftingRecipes();
        readBrewingRecipes();
    }

    @EventHandler
    public void onCraft(CraftItemEvent e) {
        ItemStack result = e.getRecipe().getResult();

        this.items.stream()
                .filter(i -> i.getType().equals(result.getType()) && i.getDurability() == result.getDurability())
                .forEach(i -> {
                    e.setCancelled(true);
                    Chat.err.send(e.getView().getPlayer(), "Du darfst dieses Item nicht craften!");
                });
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onBrew(BrewEvent e) {
        BrewerInventory inv = e.getContents();
        ItemStack ingredient = inv.getIngredient();

        if (Arrays.asList(new Material[]{Material.REDSTONE, Material.GLOWSTONE_DUST, Material.SULPHUR}).contains(
                ingredient.getType())) {
            return; // Already handled.
        }

        // Get resulting potion effect:
        PotionEffectType effect = null;
        ItemStack[] contents = inv.getContents();
        List<BrewingRecipe> recipes = BrewingRecipe.getRecipesForIngredient(ingredient);

        if (recipes.isEmpty())
            return;

        for (int i = 0; i < POTION_SLOTS; i++) {
            ItemStack base = contents[i];

            // Akward potion --> Do normal checks
            if (base.getType().equals(Material.POTION) && base.getDurability() == 16) {
                for (BrewingRecipe recipe : recipes) {
                    if (recipe.getBase().getType().equals(base.getType())
                            && recipe.getBase().getDurability() == base.getDurability()) {
                        ItemStack result = recipe.getResult();
                        short data = result.getDurability();

                        // 0x2000 = 8192 = Can become splash
                        if (data > 0x2000)
                            data -= 0x2000;

                        // 0x40 = 64 = Extended (can not be extended, instant)
                        if (data > 0x40)
                            data -= 0x40;

                        // 0x20 = 32 = Tier II (can not be strengthened)
                        if (data > 0x20)
                            data -= 0x20;

                        effect = PotionEffectType.getById(data);
                    }
                }
            }

            // Some potion with a fermented spider eye ingredient, probably.
            else {

            }
        }

        // Do final check:
        if (potions.normal.contains(effect))
            e.setCancelled(true);
    }

    @EventHandler
    public void onStrengthBrew(StrengthenBrewEvent e) {
        System.out.println("strength event fired");
        BrewerInventory inv = e.getContents();
        List<ItemStack> contents = Arrays.asList(inv.getContents());

        for (int i = 0; i < POTION_SLOTS; i++) {
            ItemStack potion = contents.get(i);
            if (potion == null)
                continue; // Slot empty.

            System.out.println("potion detected");
            PotionMeta meta = (PotionMeta) potion.getItemMeta();
            System.out.println(meta);
            System.out.println(meta.getCustomEffects());
            for (PotionEffectType effect : potions.strengthened) {
                System.out.println(effect);
                if (meta.hasCustomEffect(effect))
                    e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onExtendBrew(ExtendBrewEvent e) {
        BrewerInventory inv = e.getContents();
        List<ItemStack> contents = Arrays.asList(inv.getContents());

        for (int i = 0; i < POTION_SLOTS; i++) {
            ItemStack potion = contents.get(i);
            if (potion == null)
                continue; // Slot empty.

            PotionMeta meta = (PotionMeta) potion.getItemMeta();
            for (PotionEffectType effect : potions.extended) {
                if (meta.hasCustomEffect(effect))
                    e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onThrowableBrew(ThrowableBrewEvent e) {
        BrewerInventory inv = e.getContents();
        List<ItemStack> contents = Arrays.asList(inv.getContents());

        for (int i = 0; i < POTION_SLOTS; i++) {
            ItemStack potion = contents.get(i);
            if (potion == null)
                continue; // Slot empty.

            PotionMeta meta = (PotionMeta) potion.getItemMeta();
            for (PotionEffectType effect : potions.throwable) {
                if (meta.hasCustomEffect(effect))
                    e.setCancelled(true);
            }
        }
    }

    // === READ CONFIG.YML ===

    @SuppressWarnings("unchecked")
    private void readCraftingRecipes() {
        FileConfiguration config = VaroPlugin.getInstance().getConfig();
        String path = "blacklist.craft";

        if (!config.contains(path)) {
            return;
        }

        List<?> l = config.getList(path);

        for (String string : (List<String>) l) {
            String[] args = string.split(" ");

            try {
                Material material = Material.valueOf(args[0]);
                short dv = (args.length >= 2 ? Short.parseShort(args[1]) : 0);

                items.add(new ItemStack(material, 0, dv));
            } catch (EnumConstantNotPresentException noEnumConstant) {
                Bukkit.getLogger().log(
                        Level.SEVERE,
                        "The Material " + string + " (defined in config.yml) could not be found!"
                                + " Failed to initialize VaroPlugin!");
            } catch (NumberFormatException numberFormat) {
                Bukkit.getLogger().log(Level.SEVERE, "The string " + args[1] + " does not represent a datavalue.");
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void readBrewingRecipes() {
        FileConfiguration config = VaroPlugin.getInstance().getConfig();
        String path = "blacklist.brew";

        if (!config.contains(path)) {
            return;
        }

        String[] types = new String[]{"normal", "extended", "strengthened", "throwable"};
        for (String type : types) {
            String fullPath = path + "." + type;

            if (!config.contains(fullPath))
                continue;

            List<?> rawData = config.getList(fullPath);
            List<PotionEffectType> blacklist = (List<PotionEffectType>) potions.getRespectiveListField(type);

            if (!type.equalsIgnoreCase("normal")) {
                for (String string : (List<String>) rawData) {
                    PotionEffectType effect = PotionEffectType.getByName(string);
                    if (effect != null) {
                        blacklist.add(effect);
                    }
                }
            } else {
                for (String string : (List<String>) rawData) {
                    PotionEffectType effect = PotionEffectType.getByName(string);
                    if (effect != null) {
                        blacklist.add(effect);
                    }
                }
            }
        }
    }

    private class PotionBlacklist {

        public final List<PotionEffectType> normal = new ArrayList<PotionEffectType>();
        public final List<PotionEffectType> strengthened = new ArrayList<PotionEffectType>();
        public final List<PotionEffectType> extended = new ArrayList<PotionEffectType>();
        public final List<PotionEffectType> throwable = new ArrayList<PotionEffectType>();

        public List<?> getRespectiveListField(String name) {
            try {

                Class<?> c = getClass();
                Field f = c.getDeclaredField(name);
                return (List<?>) f.get(this);

            } catch (NoSuchFieldException | IllegalAccessException e) {
                Chat.err.serverBroadcast("If you ever see this message, java has messed up quite a few things. Please contact the author immeadeately!");
                e.printStackTrace();
                scheduleAlert();
            }

            return null;
        }

        private void scheduleAlert() {
            Bukkit.getScheduler().runTaskLater(VaroPlugin.getInstance(), () -> {
                Chat.err.serverBroadcast("============= VARO PLUGIN ALERT ==============");
                Chat.err.serverBroadcast("- Normally you wouldn't see this, but the    -");
                Chat.err.serverBroadcast("- ItemBlacklister has totally messed up. I'm -");
                Chat.err.serverBroadcast("- really sorry. I know there were a lot of   -");
                Chat.err.serverBroadcast("- errors in the console that just appeared.  -");
            }, 20L);
        }
    }

    // I need this class to be static as one doesn't need an instance of the ItemBlacklister class to use this class.
    @SuppressWarnings("unused")
    private static class Potion {

        private static final ItemStack akwardPotion = new ItemStack(Material.POTION, 1, (short) 16);

        public static Potion NIGHT_VISION = new Potion(PotionEffectType.NIGHT_VISION, Material.GOLDEN_CARROT, null);
        public static Potion INVISIBILITY = new Potion(PotionEffectType.INVISIBILITY, Material.AIR, null);
        public static Potion LEAPING = new Potion(PotionEffectType.JUMP, Material.RABBIT_FOOT, null);
        public static Potion FIRE_RESISTANCE = new Potion(PotionEffectType.FIRE_RESISTANCE, Material.MAGMA_CREAM, null);
        public static Potion SLOWNESS = new Potion(PotionEffectType.SLOW, Material.AIR, null);
        public static Potion SWIFTNESS = new Potion(PotionEffectType.SPEED, Material.SUGAR, null);
        public static Potion WATER_BREATHING = new Potion(PotionEffectType.WATER_BREATHING, Material.RAW_FISH, null);
        public static Potion HEALING = new Potion(PotionEffectType.HEAL, Material.SPECKLED_MELON, null);
        public static Potion HARMING = new Potion(PotionEffectType.HARM, Material.AIR, null);
        public static Potion POISON = new Potion(PotionEffectType.POISON, Material.SPIDER_EYE, null);
        public static Potion REGENERATION = new Potion(PotionEffectType.REGENERATION, Material.GHAST_TEAR, null);
        public static Potion STRENGTH = new Potion(PotionEffectType.INCREASE_DAMAGE, Material.BLAZE_POWDER, null);
        public static Potion WEAKNESS = new Potion(PotionEffectType.WEAKNESS, Material.FERMENTED_SPIDER_EYE, null);

        public Potion(PotionEffectType type, Object n, Object c) {

        }

        public Material getIngredient() {
            return null;
        }
    }

}
