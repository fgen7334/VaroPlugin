package de.darcoweb.varoplugin.listeners;

import org.bukkit.entity.ArmorStand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

/**
 * At the moment, this class only serves for testing purposes.
 *
 * @author 1Darco1
 */
public class SpawnsListener implements Listener {

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        if (event.getEntity() instanceof ArmorStand) {
            System.out.println("armor stand died at:");
            System.out.println(event.getEntity().getLocation());
        }
    }
}
