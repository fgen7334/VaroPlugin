package de.darcoweb.varoplugin.runnables;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.GameState;
import de.darcoweb.varoplugin.utilities.Team;
import de.darcoweb.varoplugin.utilities.TimerTuple;
import de.darcoweb.varoplugin.utilities.manager.BanManager;
import de.darcoweb.varoplugin.utilities.manager.TimerManager;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.List;

public class DailyTimer implements Runnable {

	private VaroPlugin plugin = VaroPlugin.getInstance();

	private final int checkForEnemiesTime;
	private final int checkForEnemiesRange;

	public DailyTimer() {
		this.checkForEnemiesTime = plugin.getConfig().getInt("combat.extra_time");
		this.checkForEnemiesRange = plugin.getConfig().getInt("combat.no_kick_range");
	}

	// This is run once every second, if there is no lag.
	@Override
	public void run() {
		if (plugin.isState(GameState.RUNNING) || plugin.isState(GameState.INITIAL_GRACE_PERIOD)) {
			TimerManager timerManager = plugin.getTimerManager();
			if (!timerManager.getTracked().values().isEmpty()) {
				for (TimerTuple value : timerManager.getTracked().values()) {

					OfflinePlayer offlinePlayer = value.getPlayer();
					if (offlinePlayer.isOnline()) {
						Player p = offlinePlayer.getPlayer();
						Integer invinciblePeriod = value.getInvincibleLeft();
						Integer timeLeft = value.getTimeLeft();

						// Handle invincibility when joining...
						if (invinciblePeriod >= 0) {
							if (invinciblePeriod == 0) {
								Chat.info.serverBroadcast(ChatColor.YELLOW + p.getName() + ChatColor.AQUA
										+ " ist nun verwundbar!");
								// this.invincible = false;
								// as invinciblePeriod reaches -1 the player
								// automatically gets vulnerable.
							} else if (invinciblePeriod == 30 || invinciblePeriod == 15
									|| (invinciblePeriod <= 5 && invinciblePeriod != 4)) {
								Chat.info.broadcast(ChatColor.YELLOW + p.getName() + " ist in " + invinciblePeriod
										+ " Sekunden verwundbar!");
							}
							invinciblePeriod--;
							value.setInvincibleLeft(invinciblePeriod);
						}

						// If fighting, give them more time.
						if (timeLeft < checkForEnemiesTime) {
							List<Entity> nearbyEntities = p.getNearbyEntities(checkForEnemiesRange,
									checkForEnemiesRange, checkForEnemiesRange);
							if (!nearbyEntities.isEmpty()) {
								boolean foundEnemy = false;
								for (Entity entity : nearbyEntities) {
									if (entity instanceof Player) {
										Player pNearby = (Player) entity;
										Team team = VaroPlugin.getInstance().getTeamManager().getPlayersTeam(p);

										// When Spectators will be implemented,
										// this
										// can't
										// stay like it is now. They would block
										// autokick.
										// if (TeamManager.isSpectator(pNearby)
										// ...
										if (!team.hasPlayer(pNearby.getUniqueId())) {
											timeLeft = checkForEnemiesTime;
											value.setTimeLeft(timeLeft);
											foundEnemy = true;
											break;
										}
									}
								}
								if (foundEnemy)
									break;
							}
						}

						if (timeLeft == 60 || timeLeft == 15 || timeLeft == 5) {
							broadcastTimeLeftOnServer(p);
						} else if (timeLeft == 0) {
							Chat.info.broadcast(ChatColor.YELLOW + p.getName() + Chat.info.color()
									+ " wird nun gekickt!");

							VaroPlugin.getInstance().getBanManager().addBan(p, BanManager.PLAYED_TODAY);
							int timePerDay = plugin.getConfig().getInt("playing_each_day.max_daily_time");
							p.kickPlayer(ChatColor.RED + "Du hast nun " + timePerDay
									+ " Minuten gespielt! Komm morgen wieder um weiter zu spielen!");

							VaroPlugin.getInstance().getTimerManager().removeTrackedPlayer(p);
						}
						timeLeft--;
						value.setTimeLeft(timeLeft);
						VaroPlugin.getInstance().getScoreboardManager().updateBoard(p.getUniqueId());
					}
				}
			}
		}
	}

	private void broadcastTimeLeftOnServer(OfflinePlayer p) {
		TimerManager timerManager = plugin.getTimerManager();
		int timeLeft = timerManager.getValueForPlayer(p).getTimeLeft();

		Chat.info.broadcast(ChatColor.YELLOW + p.getName() + Chat.info.color() + " darf noch " + timeLeft
				+ " Sekunden spielen!");
	}
}
