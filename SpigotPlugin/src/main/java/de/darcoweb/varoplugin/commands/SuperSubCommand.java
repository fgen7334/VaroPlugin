package de.darcoweb.varoplugin.commands;

import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import lombok.Getter;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public abstract class SuperSubCommand<T extends JavaPlugin> extends SubCommand<T> implements SuperCommand {

    @Getter//(onMethod = @__(@Override))
    protected List<SubCommand> subCommands = new ArrayList<>();

    public SuperSubCommand(Command<T> rootCommand, SuperCommand superCommand, String name, String description, String... params) {
        super(rootCommand, superCommand, name, description, params);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        // TODO help should be implemented differently, i think
        String subCommandName = args.length == 0 ? "help" : args[0];
        Optional<SubCommand> subCommandOptional = subCommands.stream()
                .filter(command -> command.getName().equals(subCommandName)).findFirst();
        if (subCommandOptional.isPresent()) {
            subCommandOptional.get().execute(sender,
                    args.length > 1 ? Arrays.copyOfRange(args, 1, args.length) : new String[0]);
        } else
            // Not sure about this. Seems okay for this project, may need a signature change to return boolean...
            Chat.err.send(sender, Message.general.INVALID_ARGUMENTS());
    }
}
