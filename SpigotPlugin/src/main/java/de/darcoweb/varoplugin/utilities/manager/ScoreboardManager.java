package de.darcoweb.varoplugin.utilities.manager;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.runnables.StartTimer;
import de.darcoweb.varoplugin.utilities.GameState;
import de.darcoweb.varoplugin.utilities.Team;
import de.darcoweb.varoplugin.utilities.TimerTuple;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;
import java.util.UUID;

public class ScoreboardManager {

    private final String objectiveName = "varoPlugin";

    private HashMap<UUID, Scoreboard> boards = new HashMap<>();

    /**
     * Registers a new Scoreboard for the given player if none exists.
     *
     * @param uuid The Unique ID for the Player to set the board up for.
     */
    public void registerBoard(UUID uuid) {
        if (!boards.containsKey(uuid)) {
            Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
            Objective o = sb.registerNewObjective(objectiveName, "dummy");
            setupObjective(uuid, o);

            boards.put(uuid, sb);
        }
        Bukkit.getPlayer(uuid).setScoreboard(boards.get(uuid));
    }

    public void unregister(UUID uuid) {
        boards.remove(uuid);
    }

    public void registerBoards() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            registerBoard(p.getUniqueId());
        }
    }

    public void updateBoard(UUID uuid) {
        Scoreboard sb = boards.get(uuid);
        sb.getObjective(objectiveName).unregister();

        Objective o = sb.registerNewObjective(objectiveName, "dummy");
        setupObjective(uuid, o);
    }

    public void updateBoards() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            updateBoard(p.getUniqueId());
        }
    }

    private void setupObjective(UUID uuid, Objective o) {
        GameState currentState = VaroPlugin.getInstance().getState();

        o.setDisplayName(ChatColor.BOLD + "VARO" + " - " + currentState.getRawName());
        // Change back to getName() maybe as it returns the color as well...

        Team team = VaroPlugin.getInstance().getTeamManager().getPlayersTeam(Bukkit.getOfflinePlayer(uuid));
        String teamName = (team == null ? "No team." : team.getName());
        int l = 0;

        switch (currentState) {
            case PREPARATION:
                l = 12;

                o.getScore(ChatColor.GRAY + "This server is currently").setScore(l--);
                o.getScore(ChatColor.GRAY + "being prepared for VARO.").setScore(l--);
                break;
            case INITIAL_GRACE_PERIOD:
                l = 12;
                String graceLeft = String.format("%02d sec.", StartTimer.getGraceLeft());

                o.getScore(ChatColor.GOLD + "" + ChatColor.BOLD + "Grace period:").setScore(l--);
                o.getScore(graceLeft).setScore(l--);
                break;
            case RUNNING:
                l = 12;
                TimerTuple tuple = VaroPlugin.getInstance().getTimerManager().getValueForPlayer(uuid);
                String timeLeftStr = String.format("%02d:%02d", tuple.getTimeLeft() / 60,
                        tuple.getTimeLeft() % 60);

                o.getScore(ChatColor.GOLD + "" + ChatColor.BOLD + "Session-Time:").setScore(l--);
                o.getScore(timeLeftStr).setScore(l--);
                break;
            case ENDED:
                Team winningTeam = VaroPlugin.getInstance().getTeamManager().getAliveTeams().iterator().next();
                o.getScore(ChatColor.GOLD + "" + ChatColor.BOLD + "Congratulations to").setScore(l--);
                o.getScore(winningTeam.getName()).setScore(l--);
                break;
        }
        o.getScore(StringUtils.repeat(" ", l)).setScore(l--); // This is not the best way of doing it (TODO)

        o.getScore(ChatColor.GREEN + "" + ChatColor.BOLD + "Team:").setScore(l--);
        o.getScore(teamName).setScore(l--);
        o.getScore(StringUtils.repeat(" ", l)).setScore(l--);

        o.getScore(ChatColor.AQUA + "" + ChatColor.BOLD + "Commands:").setScore(l--);
        o.getScore("/varo help").setScore(l--);
        o.getScore(StringUtils.repeat(" ", l)).setScore(l--);

        o.getScore(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Made by:").setScore(l--);
        o.getScore(VaroPlugin.getInstance().getDescription().getAuthors().toString()).setScore(l);

        o.setDisplaySlot(DisplaySlot.SIDEBAR);
    }
}
