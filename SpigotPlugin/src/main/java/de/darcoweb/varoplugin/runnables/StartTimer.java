package de.darcoweb.varoplugin.runnables;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.GameState;
import org.bukkit.*;
import org.bukkit.Note.Tone;
import org.bukkit.entity.Player;

public class StartTimer implements Runnable {

    private static StartTimer instance;

    public static int getGraceLeft() {
        return instance.gracePeriodLeft;
    }

    private VaroPlugin plugin;

    private int timeUntilStart = 30;
    private int gracePeriodLeft = 15;

    public StartTimer(VaroPlugin plugin) {
        this.plugin = plugin;
        this.timeUntilStart = plugin.getConfig().getInt("starting_the_game.start_countdown");
        this.gracePeriodLeft = plugin.getConfig().getInt("starting_the_game.grace_period");
        instance = this;
    }

    // This will be called once every second, if there is no lag!
    @Override
    public void run() {
        if (plugin.isState(GameState.PREPARATION)) {
            if (timeUntilStart == 0) {
                Chat.success.broadcast("Lasset die Spiele beginnen!");

                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.setGameMode(GameMode.SURVIVAL);
                    player.setHealth(20.0);
                    player.setFoodLevel(20);
                    player.setLevel(0);
                    player.setExp(0);
                    player.getWorld().setTime(0);

                    if (plugin.getTeamManager().getPlayersTeam(player) == null) {
                        Bukkit.getScheduler().runTaskLater(plugin, () ->
                                player.kickPlayer(ChatColor.RED + "Du bist nicht registriert, also in keinem Team!"), 1L);
                    }
                }

                if (gracePeriodExists()) {
                    plugin.setState(GameState.INITIAL_GRACE_PERIOD);
                    Chat.info.broadcast("Die Friedensphase startet jetzt! " + gracePeriodLeft
                            + " Sekunden Unverwundbarkeit!");
                    playSound(Sound.EXPLODE);
                } else {
                    plugin.setState(GameState.RUNNING);
                }

                addTrackedPlayers();

            } else if (timeUntilStart % 5 == 0 || (timeUntilStart < 5 && timeUntilStart != 4))
                this.broadcastTimeLeftUntilStart();

            timeUntilStart--;
            VaroPlugin.getInstance().getScoreboardManager().updateBoards();

        } else if (plugin.isState(GameState.INITIAL_GRACE_PERIOD)) {
            if (gracePeriodLeft == 0)
                plugin.setState(GameState.RUNNING);
            else if (gracePeriodLeft % 5 == 0 || (gracePeriodLeft < 5 && gracePeriodLeft != 4))
                this.broadcastTimeGracePeriodLeft();

            gracePeriodLeft--;
            VaroPlugin.getInstance().getScoreboardManager().updateBoards();
        }

        if (plugin.isState(GameState.RUNNING)) {
            Chat.custom.broadcast(ChatColor.RED + "Alle Spieler sind nun verwundbar!");
            playSound(Sound.ENDERDRAGON_GROWL);
            VaroPlugin.getInstance().getTimerManager().cancelStartTimer();
        }
    }

    private void addTrackedPlayers() {
        Bukkit.getOnlinePlayers().forEach(
                player -> VaroPlugin.getInstance().getTimerManager().newTrackedPlayer(plugin, player, false));
    }

    private void broadcastTimeGracePeriodLeft() {
        Chat.info.broadcast("Die Friedensphase endet in " + gracePeriodLeft + " Sekunden!");

        Note note = Note.sharp(0, Tone.G);
        Bukkit.getOnlinePlayers().forEach(player ->
                player.playNote(player.getLocation(), Instrument.PIANO, note));
    }

    private void broadcastTimeLeftUntilStart() {
        Chat.info.broadcast("Varo beginnt in " + timeUntilStart + " Sekunden!");

        Note note = Note.sharp(0, Tone.E);
        for (Player p : Bukkit.getOnlinePlayers()) {
            p.playNote(p.getLocation(), Instrument.PIANO, note);
        }
    }

    private void playSound(Sound sound) {
        for (Player p : Bukkit.getOnlinePlayers())
            p.playSound(p.getLocation(), sound, 1, 1);
    }

    private boolean gracePeriodExists() {
        return this.gracePeriodLeft >= 0;
    }
}
