package de.darcoweb.varoplugin.utilities;

import java.util.Optional;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class StringUtils extends org.apache.commons.lang.StringUtils {

    public static Optional<Integer> parseUnsignedInteger(String string) {
        if (string == null || string.isEmpty())
            return Optional.empty();

        int length = string.length(), i = 0;
        if (string.startsWith("-")) {
            if (length == 1)
                return Optional.empty();
            i = 1;
        }

        for (; i < length; i++) {
            char c = string.charAt(i);
            if (c < '0' || c > '9')
                return Optional.empty();
        }

        try {
            return Optional.ofNullable(Integer.parseInt(string));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

    public static Optional<Integer> parseSignedInteger(String string) {
        if (string == null || string.isEmpty())
            return Optional.empty();

        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            if (c < '0' || c > '9')
                return Optional.empty();
        }

        try {
            return Optional.ofNullable(Integer.parseInt(string));
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

    public static Optional<Integer> parseMinecraftBlockCoord(String string) {
        if (string.length() > String.valueOf(-30000000).length()) return Optional.empty();

        Optional<Integer> integerOptional = parseUnsignedInteger(string);

        if (!integerOptional.isPresent()) return integerOptional;

        int value = integerOptional.get();
        return value < -30000000 || value > 30000000 ? Optional.empty() : integerOptional;
    }
}
