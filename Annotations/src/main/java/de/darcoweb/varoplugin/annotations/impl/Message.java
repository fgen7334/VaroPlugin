package de.darcoweb.varoplugin.annotations.impl;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class Message {

    private final String name;
    private final String key;
    private final String[] params;

    public Message(String name, String key, String... params) {
        this.name = name;
        this.key = key;
        this.params = params;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public String[] getParams() {
        return params;
    }

}
