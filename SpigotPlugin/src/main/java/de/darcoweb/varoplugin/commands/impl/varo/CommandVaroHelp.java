package de.darcoweb.varoplugin.commands.impl.varo;

import de.darcoweb.varoplugin.VaroPlugin;
import de.darcoweb.varoplugin.commands.Command;
import de.darcoweb.varoplugin.commands.SubCommand;
import de.darcoweb.varoplugin.commands.SuperCommand;
import de.darcoweb.varoplugin.utilities.Chat;
import de.darcoweb.varoplugin.utilities.Message;
import org.bukkit.command.CommandSender;

/**
 * Class description here.
 *
 * @author 1Darco1
 */
public class CommandVaroHelp extends SubCommand<VaroPlugin> {

    public CommandVaroHelp(Command<VaroPlugin> rootCommand, SuperCommand superCommand) {
        super(rootCommand, superCommand, "help", "Displays the help page(s) [pages not quite]");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        StringBuilder helpBuilder = new StringBuilder();
        helpBuilder.append("#-#-#-# ")
                .append(Message.caption.HELP(rootCommand.getPlugin().getDescription().getVersion()))
                .append(" #-#-#-#").append("\n");

        rootCommand.getSubCommands().forEach(subCommand -> helpBuilder.append(buildHelp(subCommand, "varo")));

        Chat.custom.sendRaw(sender, helpBuilder.toString().trim());
    }

    private String buildHelp(SubCommand subCommand, String fullCommand) {
        fullCommand += " " + subCommand.getName();
        StringBuilder builder = new StringBuilder();

        builder.append("* /").append(fullCommand);

        for (String parameter : subCommand.getParameters()) {
            builder.append(" ");
            if (parameter.startsWith("#"))
                builder.append("[").append(parameter.substring(1)).append("]");
            else
                builder.append("<").append(parameter).append(">");
        }

        builder.append(" - ").append(subCommand.getDescription()).append("\n");

        if (subCommand instanceof SuperCommand)
            for (SubCommand nextCommand : ((SuperCommand) subCommand).getSubCommands())
                builder.append(buildHelp(nextCommand, fullCommand));

        return builder.toString();
    }
}
